package mt.edu.mcast.smsauth;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AuthenticatedNumber {
    private String number;
    private String dateTime;

    public AuthenticatedNumber(String data) {
        String[] parts = data.split(",");
        this.number = parts[0];
        DateFormat formatter = new SimpleDateFormat("MMMM d, yyyy 'at' h:mm a");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(parts[1]));
        this.dateTime = formatter.format(calendar.getTime());
    }

    public String getNumber() {
        return number;
    }

    public String getDateTime() {
        return dateTime;
    }
}
