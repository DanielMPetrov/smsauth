package mt.edu.mcast.smsauth;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuthenticatedNumbersAdapter extends RecyclerView.Adapter<AuthenticatedNumbersAdapter.ViewHolder> {

    private ArrayList<AuthenticatedNumber> authenticatedNumbers;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvNumber) TextView number;
        @BindView(R.id.tvDateTime) TextView dateTime;

        public ViewHolder(View deviceItemView) {
            super(deviceItemView);
            ButterKnife.bind(this, deviceItemView);
        }
    }

    public AuthenticatedNumbersAdapter(ArrayList<AuthenticatedNumber> authenticatedNumbers) {
        this.authenticatedNumbers = authenticatedNumbers;
    }

    @NonNull
    @Override
    public AuthenticatedNumbersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View deviceView = inflater.inflate(R.layout.authenticated_number_item, parent, false);

        return new ViewHolder(deviceView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AuthenticatedNumber authenticatedNumber = authenticatedNumbers.get(position);

        holder.number.setText(authenticatedNumber.getNumber());
        holder.dateTime.setText(authenticatedNumber.getDateTime());
    }

    @Override
    public int getItemCount() {
        return authenticatedNumbers.size();
    }
}
