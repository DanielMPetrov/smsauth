package mt.edu.mcast.smsauth;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mt.edu.mcast.smsauth.services.FileService;
import mt.edu.mcast.smsauth.services.VerificationService;
import mt.edu.mcast.smsauth.support.Permissions;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.textInputLayout) TextInputLayout mobileNumberLayout;

    @BindView(R.id.fab) FloatingActionButton fab;

    private Snackbar snackbarNoConnection;

    private VerificationService verificationService;

    private FileService fileService;

    @BindView(R.id.rvAuthenticatedNumbers) RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));

        snackbarNoConnection = Snackbar.make(toolbar,
            "Authenticator requires network connection",
            Snackbar.LENGTH_INDEFINITE);

        if (!isConnected()) {
            updateUiDisconnected();
        }

        String[] permissions = {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.SEND_SMS
        };
        if (!Permissions.hasAll(this, permissions)) {
            requestPermissions(permissions, 1);
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(Telephony.Sms.Intents.SMS_RECEIVED_ACTION);
        registerReceiver(broadcastReceiver, filter);

        fileService = new FileService(this);
        verificationService = new VerificationService(this, (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE));
        new ReadFromFileAsyncTask(fileService, rv, false).execute();
    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(action)) {
                if (isConnected()) {
                    updateUiConnected();
                } else {
                    updateUiDisconnected();
                }
                return;
            }

            if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(action)) {
                verificationService.handleSmsReceived(intent);
                new ReadFromFileAsyncTask(fileService, rv, true).execute();
                return;
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    @SuppressWarnings("Hardwareids")
    @OnClick(R.id.fab)
    public void action(View view) {
        String[] permissions = {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.SEND_SMS
        };
        if (!Permissions.hasAll(this, permissions)) {
            requestPermissions(permissions, 1);
            return;
        }

        String mobile = mobileNumberLayout.getEditText().getText().toString();
        if (mobile.length() == 0) {
            mobileNumberLayout.setError("Don't forget to supply a number");
            return;
        }

        mobileNumberLayout.setError(null);

        verificationService.sendVerificationSms(mobile);
        Toast.makeText(this, "Sending message...", Toast.LENGTH_LONG).show();
        mobileNumberLayout.getEditText().setText("");
    }

    private boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private void updateUiDisconnected() {
        fab.hide();
        snackbarNoConnection.show();
    }

    private void updateUiConnected() {
        fab.show();
        snackbarNoConnection.dismiss();
    }
}
