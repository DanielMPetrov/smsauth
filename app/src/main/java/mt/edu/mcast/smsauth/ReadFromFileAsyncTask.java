package mt.edu.mcast.smsauth;

import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import mt.edu.mcast.smsauth.services.FileService;

public class ReadFromFileAsyncTask extends AsyncTask<Void, Void, ArrayList<AuthenticatedNumber>> {

    private final WeakReference<RecyclerView> rvReference;

    private final FileService fileService;

    private final Boolean swap;

    public ReadFromFileAsyncTask(FileService fileService, RecyclerView recyclerView, Boolean swap) {
        this.rvReference = new WeakReference<>(recyclerView);
        this.fileService = fileService;
        this.swap = swap;
    }

    @Override
    protected ArrayList<AuthenticatedNumber> doInBackground(Void... voids) {
        return fileService.readAuthenticatedNumbers();
    }

    @Override
    protected void onPostExecute(ArrayList<AuthenticatedNumber> numbers) {
        RecyclerView rv = rvReference.get();

        if (rv == null) {
            return;
        }

        if (swap) {
            rv.swapAdapter(new AuthenticatedNumbersAdapter(numbers), false);
        } else {
            rv.setAdapter(new AuthenticatedNumbersAdapter(numbers));
        }
    }
}
