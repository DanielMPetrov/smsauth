package mt.edu.mcast.smsauth;

import android.os.AsyncTask;

import mt.edu.mcast.smsauth.services.FileService;

public class WriteToFileAsyncTask extends AsyncTask<String, Void, Void> {

    private final FileService fileService;

    public WriteToFileAsyncTask(FileService fileService) {
        this.fileService = fileService;
    }

    @Override
    protected Void doInBackground(String... senders) {
        fileService.writeAuthenticatedNumber(senders[0]);
        return null;
    }
}
