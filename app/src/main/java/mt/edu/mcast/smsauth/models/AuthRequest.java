package mt.edu.mcast.smsauth.models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.Date;

@IgnoreExtraProperties
public class AuthRequest {
    public long timestamp;
    public String mobile;
    public String imsi;

    public AuthRequest() {
        // Default constructor required
    }

    public AuthRequest(String mobile, String imsi) {
        this.timestamp = new Date().getTime() / 1000;
        this.mobile = mobile.replaceAll("[\\D]", "");
        this.imsi = imsi;
    }
}
