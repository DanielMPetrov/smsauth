package mt.edu.mcast.smsauth.services;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

import mt.edu.mcast.smsauth.AuthenticatedNumber;

public class FileService {

    private static final String TAG = "SMSAuth";

    private static final String FILE_NAME = "authenticated.txt";

    private final Context context;

    public FileService(Context context) {
        this.context = context;
    }

    public void writeAuthenticatedNumber(String data) {
        data = data + "," + System.currentTimeMillis() + "\n";
        try {
            FileOutputStream outputStream = context.openFileOutput(FILE_NAME,
                Context.MODE_PRIVATE | Context.MODE_APPEND);
            outputStream.write(data.getBytes());
            outputStream.close();
        } catch (Exception e) {
            Log.e(TAG, "witeAuthenticatedNumber failed");
            e.printStackTrace();
        }
    }

    public ArrayList<AuthenticatedNumber> readAuthenticatedNumbers() {
        ArrayList<AuthenticatedNumber> res = new ArrayList<>();

        try {
            FileInputStream is = new FileInputStream(new File(context.getFilesDir(), FILE_NAME));
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line = reader.readLine();
            while(line != null){
                res.add(new AuthenticatedNumber(line));
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "readAuthenticatedNumbers failed");
        }

        Collections.reverse(res);

        return res;
    }
}
