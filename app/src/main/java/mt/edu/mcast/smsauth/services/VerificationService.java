package mt.edu.mcast.smsauth.services;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import mt.edu.mcast.smsauth.WriteToFileAsyncTask;
import mt.edu.mcast.smsauth.models.AuthRequest;

public class VerificationService {

    private static final String TAG = "SMSAuth";

    private final Context context;

    private final TelephonyManager telephonyManager;

    private final FileService fileService;

    private final DatabaseReference authRef = FirebaseDatabase.getInstance().getReference("auth");

    public VerificationService(Context context, TelephonyManager telephonyManager) {
        this.context = context;
        this.telephonyManager = telephonyManager;
        this.fileService = new FileService(context);
    }

    public void sendVerificationSms(String number) {
        String imsi = getImsi();
        if (imsi == null) {
            return;
        }
        authRef.push().setValue(new AuthRequest(number, imsi));
        sendSMS(number, String.format("SA-%s is your SMS Auth verification code.", imsi));
    }

    public void handleSmsReceived(Intent intent) {
        final SmsMessage[] messages = Telephony.Sms.Intents.getMessagesFromIntent(intent);
        SmsMessage message = messages[0];
        final String body = message.getMessageBody();
        final String sender = message.getOriginatingAddress();
        Log.i(TAG, "From: " + sender + " Message: " + body);

        if (isVerificationSms(body)) {
            handleVerificationSms(sender, body);
            return;
        }

        if (isVerifiedSms(body)) {
            new WriteToFileAsyncTask(fileService).execute(sender);
            return;
        }
    }

    private void handleVerificationSms(final String sender, final String body) {
        final String code = body.split(" ")[0].split("-")[1];
        Log.i(TAG, "Code: [" + code + "]");

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Log.w(TAG, "handleVerificationSms failed due to missing READ_PHONE_STATE permission");
            return;
        }
        String myNumber = telephonyManager.getLine1Number().replaceAll("[\\D]", "");
        Log.i(TAG, myNumber);

        authRef.orderByChild("mobile").equalTo(myNumber).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                AuthRequest value = dataSnapshot.getValue(AuthRequest.class);
                // if there exists record with our number and the code we received
                if (value != null && code.equals(value.imsi)) {
                    sendVerifiedSms(sender, code);
                    authRef.removeEventListener(this);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) { }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) { }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) { }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) { }
        });
    }

    private void sendSMS(String number, String message) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(number, null, message, null, null);
        } else {
            Log.e(TAG, "Missing permission SEND_SMS");
        }
    }

    private void sendVerifiedSms(final String sender, final String code) {
        sendSMS(sender, String.format("SA-%s Verified", code));
    }

    private boolean isVerificationSms(String body) {
        return body.matches("^SA-.+ is your SMS Auth verification code\\.$");
    }

    private boolean isVerifiedSms(String body) {
        String imsi = getImsi();
        if (imsi == null) {
            return false;
        }
        return body.equals(String.format("SA-%s Verified", imsi));
    }

    private String getImsi() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Log.w(TAG, "Cannot get IMSI due to missing READ_PHONE_STATE permission");
            return null;
        }

        return telephonyManager.getSubscriberId();
    }
}
